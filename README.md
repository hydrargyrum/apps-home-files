# apps-home-files

[`apps-home-files.yml`](apps-home-files.yml) lists the various paths owned by applications in a home directory.
This includes configuration files, history files, cache files, etc.

## Purpose

The initial use of this file was: when one uninstalls an application, it removes the executable files and its system read-only data files, but it lets stray files lingering in a home directory.
`apps-home-files.yml` is a way to know what more files to clean if one wants to completely purge what an app created.

## Format

In a nutshell, the file has, per application, a list of paths or wildcards owned by the application, grouped in several categories: configuration, cache, history, state.

### Version

The format described here is at version 0.1.0.

### Example

```
version: "0.1.0"            # version of the format, not the document (don't remove the quotes here)
applications:               # this is the root of the document
  APP_KEY:                  # choose a unique name for the app
    name: "app name"        # the human-readable app name
    config:                 # this section is related to config files owned by the app
      - name: "config dir"                # description of the config
        path: "$XDG_CONFIG_HOME/the_app/" # path of a config directory
      - name: "legacy config file"
        path: "~/.the_app.conf"           # path of a config file
      - path: "~/.the_app.conf.old"       # name is optional
      - wildcard: "~/.the_app.old.*"      # pass a wildcard instead of a single path
    cache:                  # this section is related to cache data owned by the app
      - name: "base cache dir"            # the same options are accepted as in "config" section
        path: "$XDG_CACHE_HOME/the_app/"
      - name: "failed thumbnails"
        path: "$XDG_CACHE_HOME/thumbnails/fail/the_app/"
    history:                # this section is related to "history" owned by the app
      # this would typically contain files containing search history
      # or recently used files in the app.
    state:
```

### Sections

Note some files and directories might be matched in multiple sections of an app.
Care must be taken for these: for example if a user wants to remove all history of app A, but the A history file is the same file as the A config file, it might be safer not to remove the file, but warn the user not all could be cleaned as a result.

#### `config`

Configuration, settings, preferences, options, whatever you call it. Probably carefully built by the user.

#### `state`

Important data (for the user)

#### `cache`

Disposable data which can be removed at any time, and the app would still work.
If those files are removed though, the app might be a bit slower to start or run because it would take some time to reconstruct the cache, partially or totally.

Typically: thumbnails, web pages cache, etc.

#### `history`

Non-essential data for the app, and rather related on how the app was used.

Typically: recently opened files/pages, search terms typed in the app, etc.


## Contributing

Anyone is open to contribute to this data.
The format can also be improved, anyone can make suggestions, the version will be incremented if the format is changed.

## License

See [Unlicense](UNLICENSE).
